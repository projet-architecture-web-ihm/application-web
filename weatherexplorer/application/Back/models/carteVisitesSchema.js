const mongoose = require('mongoose');

const carteVisitesSchema = mongoose.Schema({
    pays: { type: String, required: true },
    ville: { type: String, required: true },
    visites: { type: Number, required: true },
    temperature: { type: Number, required: true }
});

module.exports = mongoose.model('carteVisites', carteVisitesSchema);