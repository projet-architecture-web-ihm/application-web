import { Injectable } from '@angular/core';
import {catchError, map} from 'rxjs/operators';
import {Observable, throwError} from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { RequestOptions,Headers } from '@angular/http';

@Injectable({
  providedIn: 'root'
})

export class MongoService {

  constructor(private httpClient : HttpClient) { }

  // NODE - EXPRESS API
  REST_API: string = "http://localhost:3080/WeatherExplorer";


  //Permet de récupérer toutes les villes depuis la base
  GetVilles(){
    return this.httpClient.get(`${this.REST_API}`);
  }

  //Permet de mettre à jour une ville
  updateVille(json){
    return this.httpClient.put("http://localhost:3080/update",json);
  }

  //Permet de créer une ville dans la base
  creerVille(json){
    return this.httpClient.put("http://localhost:3080/creer",json);
  }

}


