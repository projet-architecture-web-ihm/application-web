import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BarreVilleComponent } from './components/barre-ville/barre-ville.component';
import { TemperatureComponent } from './components/temperature/temperature.component';
import { VideosRecettesComponent } from './components/videos-recettes/videos-recettes.component';
import { SuggestionsMusiquesComponent } from './components/suggestions-musiques/suggestions-musiques.component';
import { MapComponent } from './components/map/map.component';

@NgModule({
  declarations: [
    AppComponent,
    BarreVilleComponent,
    TemperatureComponent,
    VideosRecettesComponent,
    SuggestionsMusiquesComponent,
    MapComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
