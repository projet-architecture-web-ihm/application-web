import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'weatherexplorer';
  ipUtilisateur = '';
  constructor(private httpClient: HttpClient) { }
  ngOnInit(): void {
    this.getIpUtilisateur();

    /*const MongoClient = require('mongodb').MongoClient;
    const uri = "mongodb+srv://WeatherExplorer:GwrnnYNd4bwtpG6c@weatherexplorer.ogw0i.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
    const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
    client.connect(err => {
      const collection = client.db("test").collection("devices");
      // perform actions on the collection object
      client.close();
    });*/
  }
  getIpUtilisateur = () => {
    this.httpClient.get("https://jsonip.com/").subscribe(
      (response: any) => {
        this.ipUtilisateur = response.ip;
      },
      (error) => {
        console.log(error)
      }
    )
  }



}
