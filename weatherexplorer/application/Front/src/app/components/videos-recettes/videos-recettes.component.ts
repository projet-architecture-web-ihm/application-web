import { Component, OnInit, Input } from '@angular/core';
import { Video } from './video';
import {TemperatureComponent} from '../temperature/temperature.component';

declare var gapi: any;


@Component({
  selector: 'app-videos-recettes',
  templateUrl: './videos-recettes.component.html',
  styleUrls: ['./videos-recettes.component.css']
})
export class VideosRecettesComponent implements OnInit {

  @Input() composantTemperature: TemperatureComponent;

  listeRecettes: Array<Video> = [];

  setTempComp = (ct :TemperatureComponent ) => {
    this.composantTemperature = ct;
  }


  /**
   * Permet de créer les suggestions de recettes en fonctions d'une liste de vidéo
   * @param listeRecettes La liste de vidéo
   */
  creerRecettes = (listeRecettes: Array<Video>) => {
    this.creerUneRecette(listeRecettes[0])
    let index = 0;
    //On parcours chaque video pour créer une suggestion
    listeRecettes.forEach(element => {
      if(index){
        this.creerUneSuggestion(element, index-1)
        index += 1;
      }
      else
       index++
    });
  }

  /**
   * Permet de créer la vidéo de recette proposé en fonction d'une video
   * @param recette La vidéo recette
   */
  creerUneRecette = (recette: Video) => {
    //On accès et modifie l'html afin d'inclure de lien embed de la video
    let html = "<iframe src=https://www.youtube.com/embed/" + recette.lien + " frameborder=\"0\" allowtransparency=\"true\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen style=\"height: 100%; width: 100%\"> </iframe>";
    let tbody = document.getElementsByClassName("chart-container-2")[0];
    tbody.appendChild(document.createElement("div"));
    tbody.children[0].innerHTML = html;
  }

  /**
   * Permet de créer une suggestion
   * @param recette La video recette
   * @param index Sa place dans la liste
   */
  creerUneSuggestion = (recette: Video, index: number) => {
    //On accès et modifie l'html afin d'inclure la suggestion
    let nbMotsTitre = 6;
    let motsTitre = recette.titre.replace(/([ .,;]+)/g,'$1§sep§').split('§sep§')
    let titreRecetteAjuste = "";
    motsTitre.forEach(mot => {
      if(nbMotsTitre>0)
        titreRecetteAjuste += mot + " "
      nbMotsTitre -= 1 
    });
    if(motsTitre.length > nbMotsTitre)
      titreRecetteAjuste += "..."

    let id = "video"+index
    let html = "<td> <i class=\"fa fa-circle text-white mr-2\"></i>"+ titreRecetteAjuste + "<td>" + recette.chaine + "</td>"
    let tbody = document.getElementById("videoRecette");
    let tr = document.createElement("tr")
    tr.setAttribute("id", id)
    tbody.appendChild(tr);
    tbody.children[index].innerHTML = html;
    
  }

  constructor() { }

  /**
   * Permet de faire le traitement de la réponse de la requête
   * @param response La réponse de la requête
   */
  traitement = (response: any) => {

    this.listeRecettes = []

    //Traitement de la réponse
    for (let i = 0; i < 5; i++) {
      // console.log(i + "VIDEO", response.result.items[i].id.videoId);
      //Autres suggestion
      let video : Video ={
        titre: response.result.items[i].snippet.title,
        lien: response.result.items[i].id.videoId,
        chaine: response.result.items[i].snippet.channelTitle,
        duree: i + "VIDEO"
      }
      this.listeRecettes.push(video);
    }
    this.creerRecettes(this.listeRecettes);
    this.creerListenerRecettes()
  }

  /**
   * Permet de changer la vidéo principale par celle cliqué en suggestion
   * @param idSuivant La vidéo cliqué
   */
  changementVideoPrincipale = (idSuivant) => {
    let temp : Video;

    //Permet d'intervertir les vidéos
      temp = this.listeRecettes[0];
      this.listeRecettes[0] = this.listeRecettes[idSuivant];
      this.listeRecettes[idSuivant] = temp;

    //Permet d'actualiser l'affichage
    this.creerRecettes(this.listeRecettes);
  }
  
  /**
   * Permet de créer un action listener pour une suggestion
   * @param i La position de la suggestion
   */
  creerListenerRecette = (i) => {
    var changerVideo = this.changementVideoPrincipale;
    document.getElementById("video"+i).style.cursor = "pointer";
    document.getElementById("video"+i).addEventListener("click",function(){
      changerVideo(i+1)
    })
  }

  /**
   * Permet de créer tout les actions listeners pour changer les vidéos
   */
  creerListenerRecettes = () => {
    for(let i=0; i<4;i++){
      this.creerListenerRecette(i)
    }
  }

  
  // BAPTISTE   : AIzaSyCl5BRNfqDi3ruSAinuUshoJF5MvRyRTWA
  // MARIE-NINA : AIzaSyAqvkzxtkwMC5lJ4MMg486qa5y_eLUksEk

  /**
   * Permet de charger le client avec la clef d'api et le service voulu
   * @returns un objet youtube 
   */
  loadClient = () => {
    gapi.client.setApiKey("AIzaSyCl5BRNfqDi3ruSAinuUshoJF5MvRyRTWA");
    return gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
      .then(function () { console.log("GAPI client loaded for API"); },
        function (err) { console.error("Error loading GAPI client for API", err); });
  }
  
  /**
   * Permet d'éxécuter une requête en fonction d'un motsclef
   * @param temp La température
   * @param desc La description
   * @param ville La ville de l'utilisateurs
   * @returns un objet youtube
   */
  execute = (temp,desc,ville) => {

    let motClef = "";

    let beau = ["Soleil","Peu nuageux","Ciel voilé","Couvert"]

    if(beau.includes(desc)){
      //Ici on sait qu'on a un temps convenable => on vérifie la température rouge > bleu > vert
      if(temp > 25){
        if(Math.random() < 0.5)
          motClef = "plat froid"
        else
          motClef = "boisson froide"
      }
      else if(temp > 20)
        motClef = "de saison"
      else
        motClef = "du terroir du "+ville
    }
    else{
      //Ici on sait que le temps est plutôt mauvais on vérifie la température => vert > gris
      if(temp > 15)
        motClef = "plat chaud"
      else
        motClef = "boisson chaude"
    }

    //On affecte la fonction de traitement
    let fonction = this.traitement;
    return gapi.client.youtube.search.list({
      q: "recette " + motClef,
      part: 'id,snippet'
    })
      .then(function(response){fonction(response)}, function (err) { console.error("Execute error", err); });
  }

  ngOnInit(): void {
    gapi.load("client");

    //On doit décaler le chargement pour laisser gapi se charger 
    setTimeout(() => {  this.loadClient(); }, 500);
    //On doit décaler l'éxécution pour laisser gapi se charger avec le client
    setTimeout(() => {  this.execute(this.composantTemperature.jsonData["forecast"][0]["temp2m"],this.composantTemperature.informationMeteo[parseInt(this.composantTemperature.jsonData["forecast"][0]["weather"])],this.composantTemperature.jsonData["city"]["name"]); }, 1500);
  }

}
