import { Component, OnInit } from '@angular/core';
import {MongoService} from './../../services/mongo.service';
import * as L from 'leaflet';

declare var ol: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  latitude: number;
  longitude: number;
  zoom: number = 5;
  minZoom: number = 2;
  maxZoom: number = 7;

  ville : string;
  pays : string;
  temperature: number;
  visites : number;

  map: any;
  source: any;
  mark: any = undefined;

  retirerMark = (marker) => {
    this.map.removeLayer(marker);
  }

  /**
   * Permet de mettre la mark de la ville courante
   * @param longitude La longitude du marqueur
   * @param latitude La latitude du marqueur
   */
  mettreMark = (longitude: number, latitude: number) => {
    if(this.mark != undefined){
      this.retirerMark(this.mark);
      this.mark = undefined;
    }
    let redIcon = L.icon({
      iconUrl: "https://upload.wikimedia.org/wikipedia/commons/e/ec/RedDot.svg",
  
      iconSize:     [24, 24], // size of the icon
    });
    this.mark = L.marker([latitude, longitude], {icon : redIcon}).addTo(this.map);
  }

  /**
   * Permet d'ajouter les marks des villes les plus visités
   * @param longitude La longitude du marqueur
   * @param latitude La latitude du marqueur
   */
  ajouterMark = (longitude: number, latitude: number) => {
    let blueIcon = L.icon({
      iconUrl: "https://upload.wikimedia.org/wikipedia/commons/4/48/Bluedot.svg",
  
      iconSize:     [16, 16], // size of the icon
     });
    L.marker([latitude, longitude], {icon : blueIcon}).addTo(this.map);
  }

  /**
   * Permet de zoomer la carte sur une position et un zoom donnée
   * @param zoom Le niveau de zoom
   * @param longitude La longitude voulu
   * @param latitude La latitude voulu
   */
  setCenter = (zoom: number, longitude: number, latitude: number) => {
    let view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([longitude, latitude]));
    view.addMarker(ol.proj.fromLonLat([longitude, latitude]));
    view.setZoom(zoom);
  }

  /**
   * Permet d'afficher le map et mettre un marqueur sur la position de l'utilisateur
   * @param position la position de l'utilisateur
   */
  success = (position) => {
    this.afficherMap(this.zoom, position.coords.longitude, position.coords.latitude);
    //this.mettreMark(position.coords.longitude, position.coords.latitude);
  }

  /**
   * Permet de localiser l'utilisateur
   */
  geoFindMe = () => {
    function error() {
      console.log("KO");
    }

    if (!navigator.geolocation) {
      console.log('Geolocation is not supported by your browser');
    } else {
      console.log('Locating…');
      navigator.geolocation.getCurrentPosition(this.success, error);
    }
  }

  /**
   * Permet d'afficher la carte en fonction d'un niveau de zoom, d'une longitude et d'une latitude
   * @param zoom Le niveau de zoom
   * @param longitude La longitude voulu
   * @param latitude La latitude voulu
   */
  afficherMap = (zoom: number, longitude: number, latitude: number) => {
    this.map = L.map('map').setView([latitude, longitude], zoom);
    L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {minZoom: this.minZoom, attribution : '<a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'}).addTo(this.map);
    this.mettreMark(longitude, latitude)
  }

  /**
   * Permet d'afficher les infos des villes les plus visités et mettre un marqueur sur la map
   */
  afficheVilleLesPlusVisitees = () =>{
    this.mongoService.GetVilles().subscribe(res => {
      let nbVilles = 0
      let villesLesplusVisitees = []
      let max = -1;
      let indiceMax = 0;

      //[1,10]
      for(nbVilles=0; nbVilles < 5; nbVilles++){
        for(let i=0; res[i] != undefined; i++){
          if(res[i]['visites'] > max){
            if(!villesLesplusVisitees.includes(i)){    
              max = res[i]['visites'];
              indiceMax = i;
            }
          }
        }
        max = -1
        if(!villesLesplusVisitees.includes(indiceMax)){

          let temperature = (res[indiceMax]['temperature'] / res[indiceMax]['visites']).toFixed(1)

          let html ="<tr> <td>"+res[indiceMax]['ville']+"</td> <td>"+temperature+"</td> <td>"+res[indiceMax]['visites']+"</td> </tr>";
          let tbody = document.getElementById("mapInfo")
          tbody.appendChild(document.createElement("tr"));
          tbody.children[villesLesplusVisitees.length].innerHTML = html;
          this.ajouterMark(res[indiceMax]['longitude'],res[indiceMax]['latitude']);

          villesLesplusVisitees.push(indiceMax);
        }
      }
    });
    this.map.removeLayer(this.mark);
    this.mark.addTo(this.map);
  }

  constructor(private mongoService : MongoService) { }

  ngOnInit(): void {
    setTimeout(() => { this.geoFindMe(); }, 1000);
    setTimeout(() => { this.afficheVilleLesPlusVisitees(); }, 2000);
  }

}
