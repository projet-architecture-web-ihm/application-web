export interface Musique {
    titre: string;
    artiste: string;
    album: string;
    date: string;
    duree: string;
    musique: string;
}