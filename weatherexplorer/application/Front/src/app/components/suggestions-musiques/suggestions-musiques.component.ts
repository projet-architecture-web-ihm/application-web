import { Component, OnInit, Input, NgModule } from '@angular/core';
import { Musique } from './musique';
import { TemperatureComponent } from '../temperature/temperature.component';
import {HttpClient,HttpHeaders} from '@angular/common/http';


@Component({
  selector: 'app-suggestions-musiques',
  templateUrl: './suggestions-musiques.component.html',
  styleUrls: ['./suggestions-musiques.component.css']
})
export class SuggestionsMusiquesComponent implements OnInit {

  //Contiendra la liste des musique à afficher
  listeMusique: Array<Musique> = [];

  @Input() composantTemperature: TemperatureComponent;
  //this.composantTemperature.jsonData["city"]["name"] pour récupérer la ville


  setTempComp = (ct :TemperatureComponent) => {
    this.composantTemperature = ct;
  }

  /**
   * Permet de créer un bloc de musique
   * @param musique La musique
   * @param index Sa position
   */
  creerBlocMusique = (musique: Musique, index: number) => {

    let html = "<tr> <td> <div> <iframe src=" + musique.musique + " frameborder=\"0\" allowtransparency=\"true\" allow=\"encrypted-media\" style=\"height: 25%; width: 100%\"> </iframe> </td> </div> <td>" + musique.titre + "</td>  <td>" + musique.artiste + "</td> <td>" + musique.album + "</td> <td>" + musique.date + "</td> <td>" + musique.duree + "</td> </tr>";

    let tbody = document.getElementById("suggestionsMusiques");

    tbody.appendChild(document.createElement("tr"));

    tbody.children[index].innerHTML = html;

  }

  /**
   * Permet de creer toutes les musiques
   * @param listeMusiques La liste de toutes les musiques
   */
  creerMusiques = (listeMusiques: Array<Musique>) => {
    let index = 0;
    listeMusiques.forEach(element => {
      this.creerBlocMusique(element, index)
      index += 1;
    });
  }

  /**
   * Nombre aléatoire de 0 à max
   * @param max Le max
   * @returns 
   */
  getRandomInt = (max) => {
    return Math.floor(Math.random() * max);
  }

  /**
   * Permet de récupérer les musiques spotify
   * @param temp La température
   * @param desc La description
   */
  getMusiques = (temp,desc) => {
    let token = "BQAncyw4IqBjsFLAMXBfnl7zeWcxvNX4rSyjB5z1asbpSb31R82NB50MhgydEa_9nKsGpLzHLZuxiTCNUfdlg9UFDnuytZqkEWbE1QEzK3kM1aKSNqYLUr-dCKo2JwdFisCP9_m6m8RYiVye-aPlYLhAWxUc";
    let headers = new HttpHeaders().set('Authorization','Bearer '+token)

    let motClef = "";

    let beau = ["Soleil","Peu nuageux","Ciel voilé","Couvert"]

    let tempsBeau = ["electro","metal","dubstep","house","party","rock","techno","reggae","reggaeton","summer","tango","spanish","r-n-b","latino"]

    let tempsMoyen = ["jazz","french","happy","acoustic","guitar","study"]

    let tempsMauvais = ["chill","piano","movies","pop-film","sad","romance","sleep"]

    if(beau.includes(desc)){
      //Ici on sait qu'on a un temps convenable => on vérifie la température rouge > bleu > vert
      if(temp > 25){
        motClef = tempsBeau[this.getRandomInt(tempsBeau.length)]
      }
      else if(temp > 20)
        motClef = tempsBeau[this.getRandomInt(tempsBeau.length)]
      else
        motClef = tempsMoyen[this.getRandomInt(tempsMoyen.length)]
    }
    else{
      //Ici on sait que le temps est plutôt mauvais on vérifie la température => vert > gris
      if(temp > 15)
        motClef = tempsMoyen[this.getRandomInt(tempsMoyen.length)]
      else
        motClef = tempsMauvais[this.getRandomInt(tempsMauvais.length)]
    }

    let url = "https://api.spotify.com/v1/search?q="+motClef+"&type=track&limit=50";

    this.http.get<any>(url, {headers})
        .toPromise()
        .then(data=> {
            this.listeMusique = [];
            let choix = []


            var temp;

            //Permet de tirer trois valeurs au hasards
            for(let i=0; i<3; i++){
              do{
                temp = this.getRandomInt(50)
              }while(choix.includes(temp))
              choix.push(temp)


                let seconds = Math.floor((data["tracks"]["items"][i]["duration_ms"] / 1000) % 60);
                let minutes = Math.floor((data["tracks"]["items"][i]["duration_ms"] / 1000 / 60) % 60);

                let musique: Musique = {
                  titre : data["tracks"]["items"][i]["name"],
                  artiste : data["tracks"]["items"][i]["artists"][0]["name"],
                  album : data["tracks"]["items"][i]["album"]["name"],
                  date : data["tracks"]["items"][i]["album"]["release_date"],
                  duree : minutes+":"+seconds,
                  musique : "https://open.spotify.com/embed/album/"+data["tracks"]["items"][i]["album"]["id"]
                }

                this.listeMusique.push(musique)
            }

            this.creerMusiques(this.listeMusique);

    });
  }

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    setTimeout(() => {  this.getMusiques(this.composantTemperature.jsonData["forecast"][0]["temp2m"],this.composantTemperature.informationMeteo[parseInt(this.composantTemperature.jsonData["forecast"][0]["weather"])]); }, 1500);
  }

}
