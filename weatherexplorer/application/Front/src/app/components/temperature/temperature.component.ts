import { Component, OnInit } from '@angular/core';
import { Temperature } from './temperature';
import { HttpClient } from '@angular/common/http';
import * as Chart from '../../../assets/plugins/Chart.js/Chart.min.js';
import { MongoService } from './../../services/mongo.service';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})

export class TemperatureComponent implements OnInit {

  semaine: Array<string> = [
    "Dimanche",
    "Lundi",
    "Mardi",
    "Mercredi",
    "Jeudi",
    "Vendredi",
    "Samedi"
  ]

  informationMeteo: Array<string> = [
    "Soleil",
    "Peu nuageux",
    "Ciel voilé",
    "Nuageux",
    "Très nuageux",
    "Couvert",
    "Brouillard",
    "Brouillard givrant",
    "Pluie faible",
    "Pluie modérée",
    "Pluie forte",
    "Pluie faible verglaçante",
    "Pluie modérée verglaçante",
    "Pluie forte verglaçante",
    "Bruine",
    "Neige faible",
    "Neige modérée",
    "Neige forte",
    "Pluie et neige mêlées faibles",
    "Pluie et neige mêlées modérées",
    "Pluie et neige mêlées fortes",
    "Averses de pluie locales et faibles",
    "Averses de pluie locales",
    "Averses locales et fortes",
    "Averses de pluie faibles",
    "Averses de pluie",
    "Averses de pluie fortes",
    "Averses de pluie faibles et fréquentes",
    "Averses de pluie fréquentes",
    "Averses de pluie fortes et fréquentes",
    "Averses de neige localisées et faibles",
    "Averses de neige localisées",
    "Averses de neige localisées et fortes",
    "Averses de neige faibles",
    "Averses de neige",
    "Averses de neige fortes",
    "Averses de neige faibles et fréquentes",
    "Averses de neige fréquentes",
    "Averses de neige fortes et fréquentes",
    "Averses de pluie et neige mêlées localisées et faibles",
    "Averses de pluie et neige mêlées localisées",
    "Averses de pluie et neige mêlées localisées et fortes",
    "Averses de pluie et neige mêlées faibles",
    "Averses de pluie et neige mêlées",
    "Averses de pluie et neige mêlées fortes",
    "Averses de pluie et neige mêlées faibles et nombreuses",
    "Averses de pluie et neige mêlées fréquentes",
    "Averses de pluie et neige mêlées fortes et fréquentes",
    "Orages faibles et locaux",
    "Orages locaux",
    "Orages fort et locaux",
    "Orages faibles",
    "Orages",
    "Orages forts",
    "Orages faibles et fréquents",
    "Orages fréquents",
    "Orages forts et fréquents",
    "Orages faibles et locaux de neige ou grésil",
    "Orages locaux de neige ou grésil",
    "Orages locaux de neige ou grésil",
    "Orages faibles de neige ou grésil",
    "Orages de neige ou grésil",
    "Orages de neige ou grésil",
    "Orages faibles et fréquents de neige ou grésil",
    "Orages fréquents de neige ou grésil",
    "Orages fréquents de neige ou grésil",
    "Orages faibles et locaux de pluie et neige mêlées ou grésil",
    "Orages locaux de pluie et neige mêlées ou grésil",
    "Orages fort et locaux de pluie et neige mêlées ou grésil",
    "Orages faibles de pluie et neige mêlées ou grésil",
    "Orages de pluie et neige mêlées ou grésil",
    "Orages forts de pluie et neige mêlées ou grésil",
    "Orages faibles et fréquents de pluie et neige mêlées ou grésil",
    "Orages fréquents de pluie et neige mêlées ou grésil",
    "Orages forts et fréquents de pluie et neige mêlées ou grésil",
    "Pluies orageuses",
    "Pluie et neige mêlées à caractère orageux",
    "Neige à caractère orageux",
    "Pluie faible intermittente",
    "Pluie modérée intermittente",
    "Pluie forte intermittente",
    "Neige faible intermittente",
    "Neige modérée intermittente",
    "Neige forte intermittente",
    "Pluie et neige mêlées",
    "Pluie et neige mêlées",
    "Pluie et neige mêlées",
    "Averses de grêle"
  ]

  temperature: Temperature = {
    forceVent: "0",
    temperature: "0",
    humidite: "0",
    information: "0"
  }

  public jsonData: any

  theme: any = "bg-theme1"

  constructor(private httpClient: HttpClient, private mongoService: MongoService) { }

  ngOnInit(): void {
    this.geoFindMe()
  }

  /**
   * En cas de succés pour la récupération des coordonnées de l'utilisateurs, on appelle les fonctions pour créer les données
   * @param position La position de l'utilisateurs
   */
  success = (position) => {
    this.recupererInfoTemperature(position.coords.longitude, position.coords.latitude);
    this.creerDiagrammeTemperature(position.coords.longitude, position.coords.latitude);
  }

  /**
   * Permet de récupérer les coordonnées de l'utilisateurs
   */
  geoFindMe = () => {

    function error() {
      console.log("KO");
      this.recupererInfoTemperature(2.35, 48.86);
      this.creerDiagrammeTemperature(2.35, 48.86);
    }

    if (!navigator.geolocation) {
      console.log('Geolocation is not supported by your browser');
    } else {
      console.log('Locating…');
      navigator.geolocation.getCurrentPosition(this.success, error);
    }
  }

  /**
   * Permet de récupérer les informations sur la température actuelle pour les afficher en dessous du diagramme 
   * @param longitude La longitude de l'utilisateur
   * @param latitude La latitude de l'utilisateur
   */
  public recupererInfoTemperature = function (longitude: number, latitude: number) {
    //Permet de récupérer la température sur les prochaines heures
    this.httpClient.get("https://api.meteo-concept.com/api/forecast/nextHours?token=" + "48976e879e11ca210cee4a397859dd608e9504506bc0ddb5f82316f72e3c6fdc" + "&latlng=" + latitude.toFixed(3) + "," + longitude.toFixed(3)).subscribe(
      (response: any) => {
        //On récupére les données en JSON
        this.jsonData = JSON.parse(JSON.stringify(response));

        if (this.jsonData != undefined) {
          //Permet d'ajouter ou de modifier une ville
          this.envoyerInformationVille(this.jsonData)

          //On applique les données dans un structure température
          this.temperature = {
            forceVent: this.jsonData["forecast"][0]["wind10m"],
            temperature: this.jsonData["forecast"][0]["temp2m"],
            humidite: this.jsonData["forecast"][0]["rh2m"],
            information: " ("+this.jsonData["city"]["name"] +")"+" : "+this.informationMeteo[parseInt(this.jsonData["forecast"][0]["weather"])]
          }

          //On appelle la fonction pour changer le background en fonction de la météo
          this.changerBackground(this.jsonData["forecast"][0]["temp2m"], this.informationMeteo[parseInt(this.jsonData["forecast"][0]["weather"])])

          //Permet de faire tourner la flèche de la force du vent en fonction de sa valeur
          document.getElementById("forceVent").style.transform = "rotate(" + this.jsonData["forecast"][0]["dirwind10m"] + "deg)"

          // RORATION FLECHE TEMPERATURE
          let t1 = parseInt(this.temperature.temperature)
          let t2 = parseInt(this.jsonData["forecast"][1]["temp2m"])
          if (t1 == 0) {
            t1 = 1
            t2 += 1
          }
          let degreRotation = (t2 * 45 / t1)
          if (t1 <= t2) {
            degreRotation = -(degreRotation - 45)
            if (degreRotation < -90)
              degreRotation = -90
          }
          else {
            degreRotation = 45 - degreRotation
            if (degreRotation > 90)
              degreRotation = 90
          }
          document.getElementById("temperature").style.transform = "rotate(" + degreRotation + "deg)"

          // RORATION FLECHE HUMIDITE
          let h1 = parseInt(this.temperature.humidite)
          let h2 = parseInt(this.jsonData["forecast"][1]["rh2m"])
          if (h1 == 0) {
            h1 = 1
            h2 += 1
          }
          degreRotation = (h2 * 45 / h1)
          if (h1 <= h2) {
            degreRotation = -(degreRotation - 45)
            if (degreRotation < -90)
              degreRotation = -90
          }
          else {
            degreRotation = 45 - degreRotation
            if (degreRotation > 90)
              degreRotation = 90
          }
          document.getElementById("humidite").style.transform = "rotate(" + degreRotation + "deg)"
        }
      },
      (error) => {
        console.log(error)
      }
    )
  }

  /**
   * Permet d'ajouter ou de mettre à jour les informations d'une ville
   * @param json le json des informations de la ville
   */
  envoyerInformationVille = (json) => {
    this.mongoService.GetVilles().subscribe(res => {
      let indice = -1;
      //On détecter la présence de la ville
      for (let i = 0; res[i] != undefined; i++) {
        if (res[i]['ville'] == json["city"]["name"])
          indice = i;
      }

      //Si elle existe alors on met à jour son nombre de visite ainsi que sa température moyenne
      if (indice != -1) {
        let visites = res[indice]['visites'] + 1;
        let temperature = res[indice]['temperature'] + json["forecast"][0]["temp2m"];

        let jsonUpdate = {
          ville: res[indice]['ville'],
          visites: visites,
          temperature: temperature,
          longitude: res[indice]['longitude'],
          latitude: res[indice]['latitude']
        }

        this.mongoService.updateVille(jsonUpdate).subscribe(
          (response: any) => {
            console.log("update mongo ok");
          },
          (error: any) => {
            console.log(error);
          }
        );
      }
      //On doit ici ajouter la ville
      else {

        let jsonNew = {
          ville: json["city"]["name"],
          visites: 1,
          temperature: json["forecast"][0]["temp2m"],
          longitude: json["city"]['longitude'],
          latitude: json["city"]["latitude"]
        }

        this.mongoService.creerVille(jsonNew).subscribe(
          (response: any) => {
            console.log("creer mongo ok");
          },
          (error: any) => {
            console.log(error);
          }
        );
      }
    });
  }

  //TOKEN 1 : 25e65c14485682767290a1af56c909ff865c123a20472ef462fd518aed7ecf73
  //TOKEN 2 : 48976e879e11ca210cee4a397859dd608e9504506bc0ddb5f82316f72e3c6fdc

  /**
   * Permet de créer le diagramme affichant les courbures de température maximale et minimale sous 7 jours
   * @param longitude La longitude de l'utilisateur
   * @param latitude La latitude de l'utilisateur
   */
  public creerDiagrammeTemperature = (longitude: number, latitude: number) => {
    //Permet de faire une requête pour récupérer les températuers sur plusieurs jours
    this.httpClient.get("https://api.meteo-concept.com/api/forecast/daily?token=" + "48976e879e11ca210cee4a397859dd608e9504506bc0ddb5f82316f72e3c6fdc" + "&latlng=" + latitude.toFixed(3) + "," + longitude.toFixed(3)).subscribe(
      (response: any) => {
        //On récupére les données en JSON
        this.jsonData = JSON.parse(JSON.stringify(response))

        if (this.jsonData != undefined) {
          //On créer deux tableaux qui stock les différentes températures
          let tempBasse = [];
          let tempHaute = [];

          //On créer un tableau de jours permettant d'avoir toujours en premier le jour actuelle
          let jours = []
          let d = new Date();
          for (let i = 0; i < 7; i++) {
            tempBasse.push(parseInt(this.jsonData["forecast"][i]["tmin"]))
            tempHaute.push(parseInt(this.jsonData["forecast"][i]["tmax"]))

            jours.push(this.semaine[(d.getDay() + i) % 7])
          }

          //Ici on créer un canvas qui correspond au diagramme
          var canvas: HTMLCanvasElement = <HTMLCanvasElement>document.getElementById('chart1');
          var ctx: CanvasRenderingContext2D = canvas.getContext("2d");
          var myChart = new Chart(ctx, {
            type: 'line',
            data: {
              labels: jours,
              datasets: [{
                label: 'Température la plus basse',
                data: tempBasse,
                backgroundColor: 'transparent',
                borderColor: "rgba(140, 159, 255, 1)",
                pointRadius: 3,
                borderWidth: 3
              }, {
                label: 'Température la plus haute',
                data: tempHaute,
                backgroundColor: "transparent",
                borderColor: "rgba(255, 87, 87, 0.5)",
                pointRadius: 3,
                borderWidth: 3
              }]
            },
            options: {
              maintainAspectRatio: false,
              legend: {
                display: false,
                labels: {
                  fontColor: '#ddd',
                  boxWidth: 40
                }
              },
              tooltips: {
                displayColors: false
              },
              scales: {
                xAxes: [{
                  ticks: {
                    beginAtZero: true,
                    fontColor: '#ddd'
                  },
                  gridLines: {
                    display: true,
                    color: "rgba(221, 221, 221, 0.08)"
                  },
                }],
                yAxes: [{
                  ticks: {
                    beginAtZero: true,
                    fontColor: '#ddd'
                  },
                  gridLines: {
                    display: true,
                    color: "rgba(221, 221, 221, 0.08)"
                  },
                }]
              }

            }
          });
        }
      },
      (error) => {
        console.log(error)
      })
  }


  /**
   * Permet de changer le background de la page avec la température et la description du temps
   * @param temp La température courante
   * @param descText La description du temps
   */
  changerBackground = (temp: number, descText: string) => {
    //Deux tableaux contenant les descriptions de tout les états possibles
    let beau = ["Soleil", "Peu nuageux", "Ciel voilé", "Couvert"]
    // theme 1 = bleu 
    // theme 2 = gris
    // theme 3 = vert
    // theme 6 = rouge
    //Variable contenant le theme à appliquer

    document.getElementById("background").classList.remove(this.theme)

    this.theme = "bg-theme"

    if (beau.includes(descText)) {
      //Ici on sait qu'on a un temps convenable => on vérifie la température rouge > bleu > vert
      if (temp > 25)
        this.theme = "bg-theme6"
      else if (temp > 20)
        this.theme = "bg-theme1"
      else
        this.theme = "bg-theme3"
    }
    else {
      //Ici on sait que le temps est plutôt mauvais on vérifie la température => vert > gris
      if (temp > 15)
        this.theme = "bg-theme3"
      else
        this.theme = "bg-theme2"
    }


    //Ici on applique le theme au body
    document.getElementById("background").classList.add(this.theme)
  }

}
