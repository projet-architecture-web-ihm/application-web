import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TemperatureComponent } from '../temperature/temperature.component';
import { SuggestionsMusiquesComponent } from '../suggestions-musiques/suggestions-musiques.component';
import { VideosRecettesComponent } from '../videos-recettes/videos-recettes.component';
import { MapComponent } from '../map/map.component';

@Component({
  selector: 'app-barre-ville',
  templateUrl: './barre-ville.component.html',
  styleUrls: ['./barre-ville.component.css']
})
export class BarreVilleComponent implements OnInit {

  /**
   * Permet d'importer tout les autres modules dans celui ci afin de gérer le cas de la saisie de la ville
   */
  @Input() composantTemperature: TemperatureComponent;
  @Input() composantMusiques: SuggestionsMusiquesComponent;
  @Input() composantVideos: VideosRecettesComponent;
  @Input() composantMap: MapComponent;


  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {

    var fonctionRecherche = this.rechercheCoordonnesVille;

    /**
     * Permet de gérer la détection lors de la saisie de la ville par l'utilisateurs
     */
    document.getElementById("saisieIcon").addEventListener("click",function(){
      let barre = (<HTMLInputElement>(document.getElementById("saisieText"))).value;
      if(barre.length != 0){
        fonctionRecherche(barre)
      }
    });

    document.getElementById("saisieText").addEventListener("keyup",function(e){
      let barre = (<HTMLInputElement>(document.getElementById("saisieText"))).value;
      if(e.key == 'Enter' && barre.length != 0){
        fonctionRecherche(barre)
      }
    });
  } 

  /**
   * Permet de rechercher les coordonnées de la ville saisie
   * @param ville La ville saisie
   */
  rechercheCoordonnesVille = (ville :string) => {
    let longitude :number
    let latitude :number
    var fonctionRecharge = this.rechargerElements

    //Requête pour récupérer les coordonnées de la ville
    this.httpClient.get("https://api.meteo-concept.com/api/location/cities?token=48976e879e11ca210cee4a397859dd608e9504506bc0ddb5f82316f72e3c6fdc&search="+ville).subscribe(
      (response: any) => {
        let jsonData = JSON.parse(JSON.stringify(response))
        if(jsonData["cities"][0] != undefined){
          longitude = Number(jsonData["cities"][0]['longitude'])
          latitude = Number(jsonData["cities"][0]['latitude'])
          fonctionRecharge(longitude, latitude);
        }
      },
      (error) => {
        console.log(error)
      }
    )
  }

  /**
   * Permet de recharger tout les components
   * @param longitude La longitude de la ville
   * @param latitude La latitude de la ville
   */
  rechargerElements = (longitude :number, latitude :number) => {
      this.composantTemperature.recupererInfoTemperature(longitude, latitude);
      this.composantTemperature.creerDiagrammeTemperature(longitude, latitude);
      setTimeout(() => {  this.composantVideos.loadClient(); }, 500);
      setTimeout(() => {  this.composantVideos.execute(this.composantVideos.composantTemperature.jsonData["forecast"][0]["temp2m"],this.composantVideos.composantTemperature.informationMeteo[parseInt(this.composantVideos.composantTemperature.jsonData["forecast"][0]["weather"])],this.composantVideos.composantTemperature.jsonData["city"]["name"]); }, 1500);
      setTimeout(() => { this.composantMusiques.getMusiques(this.composantMusiques.composantTemperature.jsonData["forecast"][0]["temp2m"],this.composantMusiques.composantTemperature.informationMeteo[parseInt(this.composantMusiques.composantTemperature.jsonData["forecast"][0]["weather"])]);},500);
      if(this.composantMap.map == undefined)
        setTimeout(() => { this.composantMap.afficherMap(this.composantMap.zoom,longitude, latitude);},500);
      else{
        setTimeout(() => { this.composantMap.retirerMark(this.composantMap.mark);},500);
        setTimeout(() => { this.composantMap.mettreMark(longitude, latitude);},1000);
        setTimeout(() => { this.composantMap.map.setView([latitude, longitude], this.composantMap.zoom);},1500);
        setTimeout(() => { this.composantMap.afficheVilleLesPlusVisitees();},500);
      }
  }

}
