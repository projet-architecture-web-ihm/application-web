import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'weatherexplorer';
  ipUtilisateur = '';
  constructor(private httpClient: HttpClient) { }
  ngOnInit(): void {
    this.getIpUtilisateur();

  }
  getIpUtilisateur = () => {
    this.httpClient.get("https://jsonip.com/").subscribe(
      (response: any) => {
        this.ipUtilisateur = response.ip;
      },
      (error) => {
        console.log(error)
      }
    )
  }



}
