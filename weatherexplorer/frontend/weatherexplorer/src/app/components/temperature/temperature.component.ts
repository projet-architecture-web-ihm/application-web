import { Component, OnInit } from '@angular/core';
import { Temperature } from './temperature';
import { HttpClient } from '@angular/common/http';
import * as Chart from '../../../assets/plugins/Chart.js/Chart.min.js';

@Component({
  selector: 'app-temperature',
  templateUrl: './temperature.component.html',
  styleUrls: ['./temperature.component.css']
})

export class TemperatureComponent implements OnInit {

  semaine: Array<string> = [
   "Dimanche",
   "Lundi",
   "Mardi",
   "Mercredi",
   "Jeudi",
   "Vendredi",
   "Samedi"
  ]

  informationMeteo: Array<string> = [
    "Soleil",
    "Peu nuageux",
    "Ciel voilé",
    "Nuageux", "Très nuageux",
    "Couvert",
    "Brouillard",
    "Brouillard givrant",
    "Pluie faible",
    "Pluie modérée",
    "Pluie forte",
    "Pluie faible verglaçante",
    "Pluie modérée verglaçante",
    "Pluie forte verglaçante",
    "Bruine",
    "Neige faible",
    "Neige modérée",
    "Neige forte",
    "Pluie et neige mêlées faibles",
    "Pluie et neige mêlées modérées",
    "Pluie et neige mêlées fortes",
    "Averses de pluie locales et faibles",
    "Averses de pluie locales",
    "Averses locales et fortes",
    "Averses de pluie faibles",
    "Averses de pluie",
    "Averses de pluie fortes",
    "Averses de pluie faibles et fréquentes",
    "Averses de pluie fréquentes",
    "Averses de pluie fortes et fréquentes",
    "Averses de neige localisées et faibles",
    "Averses de neige localisées",
    "Averses de neige localisées et fortes",
    "Averses de neige faibles",
    "Averses de neige",
    "Averses de neige fortes",
    "Averses de neige faibles et fréquentes",
    "Averses de neige fréquentes",
    "Averses de neige fortes et fréquentes",
    "Averses de pluie et neige mêlées localisées et faibles",
    "Averses de pluie et neige mêlées localisées",
    "Averses de pluie et neige mêlées localisées et fortes",
    "Averses de pluie et neige mêlées faibles",
    "Averses de pluie et neige mêlées",
    "Averses de pluie et neige mêlées fortes",
    "Averses de pluie et neige mêlées faibles et nombreuses",
    "Averses de pluie et neige mêlées fréquentes",
    "Averses de pluie et neige mêlées fortes et fréquentes",
    "Orages faibles et locaux",
    "Orages locaux",
    "Orages fort et locaux",
    "Orages faibles",
    "Orages",
    "Orages forts",
    "Orages faibles et fréquents",
    "Orages fréquents",
    "Orages forts et fréquents",
    "Orages faibles et locaux de neige ou grésil",
    "Orages locaux de neige ou grésil",
    "Orages locaux de neige ou grésil",
    "Orages faibles de neige ou grésil",
    "Orages de neige ou grésil",
    "Orages de neige ou grésil",
    "Orages faibles et fréquents de neige ou grésil",
    "Orages fréquents de neige ou grésil",
    "Orages fréquents de neige ou grésil",
    "Orages faibles et locaux de pluie et neige mêlées ou grésil",
    "Orages locaux de pluie et neige mêlées ou grésil",
    "Orages fort et locaux de pluie et neige mêlées ou grésil",
    "Orages faibles de pluie et neige mêlées ou grésil",
    "Orages de pluie et neige mêlées ou grésil",
    "Orages forts de pluie et neige mêlées ou grésil",
    "Orages faibles et fréquents de pluie et neige mêlées ou grésil",
    "Orages fréquents de pluie et neige mêlées ou grésil",
    "Orages forts et fréquents de pluie et neige mêlées ou grésil",
    "Pluies orageuses",
    "Pluie et neige mêlées à caractère orageux",
    "Neige à caractère orageux",
    "Pluie faible intermittente",
    "Pluie modérée intermittente",
    "Pluie forte intermittente",
    "Neige faible intermittente",
    "Neige modérée intermittente",
    "Neige forte intermittente",
    "Pluie et neige mêlées",
    "Pluie et neige mêlées",
    "Pluie et neige mêlées",
    "Averses de grêle"
  ]

  temperature: Temperature = {
    forceVent: "0",
    temperature: "0",
    humidite: "0",
    information: "0"
  }

  jsonData: any

  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {
    this.geoFindMe()
  }

  success = (position) => {
    this.recupererInfoTemperature(position.coords.longitude, position.coords.latitude);
    this.creerDiagrammeTemperature(position.coords.longitude, position.coords.latitude);
  }

  geoFindMe = () => {

    function error() {
      console.log("KO");
    }

    if (!navigator.geolocation) {
      console.log('Geolocation is not supported by your browser');
    } else {
      console.log('Locating…');
      navigator.geolocation.getCurrentPosition(this.success, error);
    }
  }

  recupererInfoTemperature = (longitude: number, latitude: number) => {

    this.httpClient.get("https://api.meteo-concept.com/api/forecast/nextHours?token=" + "25e65c14485682767290a1af56c909ff865c123a20472ef462fd518aed7ecf73" + "&latlng=" + latitude.toFixed(3) + "," + longitude.toFixed(3)).subscribe(
      (response: any) => {
        this.jsonData = JSON.parse(JSON.stringify(response))

        console.log("TESSST : " + this.jsonData["city"]["name"]);

        this.temperature = {
          forceVent: this.jsonData["forecast"][0]["wind10m"],
          temperature: this.jsonData["forecast"][0]["temp2m"],
          humidite: this.jsonData["forecast"][0]["rh2m"],
          information: this.informationMeteo[parseInt(this.jsonData["forecast"][0]["weather"])]
        }

        document.getElementById("forceVent").style.transform = "rotate(" + this.jsonData["forecast"][0]["dirwind10m"] + "deg)"

        // RORATION FLECHE TEMPERATURE
        let t1 = parseInt(this.temperature.temperature)
        let t2 = parseInt(this.jsonData["forecast"][1]["temp2m"])
        if (t1 == 0) {
          t1 = 1
          t2 += 1
        }
        let degreRotation = (t2 * 45 / t1)
        if (t1 <= t2) {
          degreRotation = -(degreRotation - 45)
          if (degreRotation < -90)
            degreRotation = -90
        }
        else {
          degreRotation = 45 - degreRotation
          if (degreRotation > 90)
            degreRotation = 90
        }
        document.getElementById("temperature").style.transform = "rotate(" + degreRotation + "deg)"

        // RORATION FLECHE HUMIDITE
        let h1 = parseInt(this.temperature.humidite)
        let h2 = parseInt(this.jsonData["forecast"][1]["rh2m"])
        if (h1 == 0) {
          h1 = 1
          h2 += 1
        }
        degreRotation = (h2 * 45 / h1)
        if (h1 <= h2) {
          degreRotation = -(degreRotation - 45)
          if (degreRotation < -90)
            degreRotation = -90
        }
        else {
          degreRotation = 45 - degreRotation
          if (degreRotation > 90)
            degreRotation = 90
        }
        document.getElementById("humidite").style.transform = "rotate(" + degreRotation + "deg)"
      },
      (error) => {
        alert(error)
      }
    )

  }

  creerDiagrammeTemperature = (longitude: number, latitude: number) => {
    this.httpClient.get("https://api.meteo-concept.com/api/forecast/daily?token=" + "25e65c14485682767290a1af56c909ff865c123a20472ef462fd518aed7ecf73" + "&latlng=" + latitude.toFixed(3) + "," + longitude.toFixed(3)).subscribe(
      (response: any) => {

        let jsonData = JSON.parse(JSON.stringify(response))

        //jsonData["forecast"][0]["temp2m"]

        let tempBasse = [];
        let tempHaute = [];
        let jours = []

        let d = new Date();
        for(let i=0; i<7; i++){
          tempBasse.push(parseInt(jsonData["forecast"][i]["tmin"]))
          tempHaute.push(parseInt(jsonData["forecast"][i]["tmax"]))

          jours.push(this.semaine[(d.getDay()+i)%7])
        }

        var canvas: HTMLCanvasElement = <HTMLCanvasElement> document.getElementById('chart1');
        var ctx: CanvasRenderingContext2D = canvas.getContext("2d");
        var myChart = new Chart(ctx, {
          type: 'line',
          data: {
            labels: jours,
            datasets: [{
              label: 'Température la plus basse',
              data: tempBasse,
              backgroundColor: 'transparent',
              borderColor: "rgba(140, 159, 255, 1)",
              pointRadius: 3,
              borderWidth: 3
            }, {
              label: 'Température la plus haute',
              data: tempHaute,
              backgroundColor: "transparent",
              borderColor: "rgba(255, 87, 87, 0.5)",
              pointRadius: 3,
              borderWidth: 3
            }]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false,
              labels: {
                fontColor: '#ddd',
                boxWidth: 40
              }
            },
            tooltips: {
              displayColors: false
            },
            scales: {
              xAxes: [{
                ticks: {
                  beginAtZero: true,
                  fontColor: '#ddd'
                },
                gridLines: {
                  display: true,
                  color: "rgba(221, 221, 221, 0.08)"
                },
              }],
              yAxes: [{
                ticks: {
                  beginAtZero: true,
                  fontColor: '#ddd'
                },
                gridLines: {
                  display: true,
                  color: "rgba(221, 221, 221, 0.08)"
                },
              }]
            }
    
          }
        });
      },
      (error) => {
        alert(error)
      }
    )
  }
}
