export interface Temperature {
    temperature: string;
    forceVent: string;
    humidite: string;
    information : string;
}