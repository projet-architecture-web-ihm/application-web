export interface Statistiques {
    nomVille: string;
    temperatureMoyenne: number;
    nombreVisites: number;
    nombreVideosGenerees: number;
    nombreMusiquesGenerees: number;
}