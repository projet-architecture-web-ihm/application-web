import { Component, OnInit } from '@angular/core';
import { Statistiques } from './statistiques';
@Component({
  selector: 'app-barre-info',
  templateUrl: './barre-info.component.html',
  styleUrls: ['./barre-info.component.css']
})
export class BarreInfoComponent implements OnInit {

  statistiques: Statistiques = {
      nomVille: "Rennes",
      nombreVisites: 145,
      temperatureMoyenne: 25.0,
      nombreVideosGenerees: 1569,
      nombreMusiquesGenerees: 1459
  };

  constructor() { }

  ngOnInit(): void {
  }

}
