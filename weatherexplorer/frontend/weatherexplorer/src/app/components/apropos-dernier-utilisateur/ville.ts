export interface Ville {
    nom: string;
    temperature: string;
    videoProposee: string;
    musiqueProposee: string;
}