import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AproposDernierUtilisateurComponent } from './apropos-dernier-utilisateur.component';

describe('AproposDernierUtilisateurComponent', () => {
  let component: AproposDernierUtilisateurComponent;
  let fixture: ComponentFixture<AproposDernierUtilisateurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AproposDernierUtilisateurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AproposDernierUtilisateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
