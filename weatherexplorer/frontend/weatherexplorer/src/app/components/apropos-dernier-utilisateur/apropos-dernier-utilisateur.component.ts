import { Component, OnInit } from '@angular/core';
import { Ville } from './ville';

@Component({
  selector: 'app-apropos-dernier-utilisateur',
  templateUrl: './apropos-dernier-utilisateur.component.html',
  styleUrls: ['./apropos-dernier-utilisateur.component.css']
})
export class AproposDernierUtilisateurComponent implements OnInit {

  ville: Ville = {
    nom: "Rennes",
    temperature: "20.5",
    videoProposee: "Recette de café",
    musiqueProposee: "Zumba cafew"
  };

  constructor() { }

  ngOnInit(): void {
  }

}
