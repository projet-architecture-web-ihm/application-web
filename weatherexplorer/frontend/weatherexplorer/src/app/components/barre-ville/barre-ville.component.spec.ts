import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BarreVilleComponent } from './barre-ville.component';

describe('BarreVilleComponent', () => {
  let component: BarreVilleComponent;
  let fixture: ComponentFixture<BarreVilleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BarreVilleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BarreVilleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
