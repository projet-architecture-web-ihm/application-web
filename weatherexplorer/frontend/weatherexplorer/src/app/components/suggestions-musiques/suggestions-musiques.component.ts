import { Component, OnInit, Input, NgModule } from '@angular/core';
import { Musique } from './musique';
import { TemperatureComponent } from '../temperature/temperature.component';
import {HttpClient} from '@angular/common/http';

/*@NgModule({
  declarations: [SuggestionsMusiquesComponent],
  imports: [NgxSpotifyApiModule],
  providers: [],
  bootstrap: [SuggestionsMusiquesComponent],
  //var app = angular.module('example', ['spotify']);
})*/

@Component({
  selector: 'app-suggestions-musiques',
  templateUrl: './suggestions-musiques.component.html',
  styleUrls: ['./suggestions-musiques.component.css']
})
export class SuggestionsMusiquesComponent implements OnInit {

  //Contiendra la liste des musique à afficher
  listeMusique: Array<Musique> = [];

  @Input() composantTemperature: TemperatureComponent;
  //this.composantTemperature.jsonData["city"]["name"] pour récupérer la ville

  //Musique principale et musique secondaire sont écrites en dur en attendant la récupération du json
  musiquePrincipale: Musique = {
    titre: "La jument de Michao",
    artiste: "Nolwenn Leroy",
    album: "Bretonne",
    date: "2010",
    duree: "2:57",
    musique: "https://open.spotify.com/embed/track/3i2NoM7ijfKyDEsjMbORDJ"
  };

  musiqueSecondaire: Musique = {
    titre: "Bretonera",
    artiste: "Kozmael",
    album: "Bretonera",
    date: "2020",
    duree: "2:27",
    musique: "https://open.spotify.com/embed/track/72DQPQJHu52J4PUFkeaZzz"
  };

  creerBlocMusique = (musique: Musique, index: number) => {

    let html = "<tr> <td>" + musique.titre + "</td> <td> <iframe src=" + musique.musique + " frameborder=\"0\" allowtransparency=\"true\" allow=\"encrypted-media\" style=\"height: 25%; width: 100%\"> </iframe> </td> <td>" + musique.artiste + "</td> <td>" + musique.album + "</td> <td>" + musique.date + "</td> <td>" + musique.duree + "</td> </tr>";

    let tbody = document.getElementById("suggestionsMusiques");

    tbody.appendChild(document.createElement("tr"));

    tbody.children[index].innerHTML = html;

  }

  creerMusique = (listeMusiques: Array<Musique>) => {
    let index = 0;
    listeMusiques.forEach(element => {
      this.creerBlocMusique(element, index)
      index += 1;
    });
  }



  getToken = () => {

  }



  constructor(private httpClient: HttpClient) { }

  ngOnInit(): void {

    let musiqueAuto: Musique = {
      titre: "CHAO",
      artiste: "Nolwenn Leroy",
      album: "Bretonne",
      date: "2010",
      duree: "2:57",
      musique: "https://open.spotify.com/embed/track/3i2NoM7ijfKyDEsjMbORDJ"
    }

    this.listeMusique.push(this.musiquePrincipale);
    this.listeMusique.push(this.musiqueSecondaire);
    this.listeMusique.push(musiqueAuto);

    this.creerMusique(this.listeMusique);

    this.getToken();
  }

}
