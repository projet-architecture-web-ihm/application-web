import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestionsMusiquesComponent } from './suggestions-musiques.component';

describe('SuggestionsMusiquesComponent', () => {
  let component: SuggestionsMusiquesComponent;
  let fixture: ComponentFixture<SuggestionsMusiquesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuggestionsMusiquesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestionsMusiquesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
