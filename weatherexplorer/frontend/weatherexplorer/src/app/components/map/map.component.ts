import { Component, OnInit } from '@angular/core';
declare var ol: any;

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  latitude: number;
  longitude: number;
  zoom: number = 3;

  map: any;

  mettreMark = (longitude: number, latitude: number) => {
    let vectorLayer = new ol.layer.Vector({
      source: new ol.source.Vector({
        features: [new ol.Feature({
          geometry: new ol.geom.Point(ol.proj.transform([longitude, latitude], 'EPSG:4326', 'EPSG:3857')),
        })]
      }),
      style: new ol.style.Style({
        image: new ol.style.Icon({
          anchor: [0.5, 0.5],
          anchorXUnits: "fraction",
          anchorYUnits: "fraction",
          src: "https://upload.wikimedia.org/wikipedia/commons/e/ec/RedDot.svg"
        })
      })
    });
    this.map.addLayer(vectorLayer);
  }

  setCenter = (zoom: number, longitude: number, latitude: number) => {
    let view = this.map.getView();
    view.setCenter(ol.proj.fromLonLat([longitude, latitude]));
    view.addMarker(ol.proj.fromLonLat([longitude, latitude]));
    view.setZoom(zoom);
  }

  success = (position) => {
    this.afficherMap(this.zoom, position.coords.longitude, position.coords.latitude);
    this.mettreMark(position.coords.longitude, position.coords.latitude);
  }

  geoFindMe = () => {

    function error() {
      console.log("KO");
    }

    if (!navigator.geolocation) {
      console.log('Geolocation is not supported by your browser');
    } else {
      console.log('Locating…');
      navigator.geolocation.getCurrentPosition(this.success, error);
    }
  }

  afficherMap = (zoom: number, longitude: number, latitude: number) => {
    this.map = new ol.Map({
      target: "map",
      layers: [
        new ol.layer.Tile({
          source: new ol.source.OSM({
            url: "http://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          })
        })
      ],
      view: new ol.View({
        center: ol.proj.fromLonLat([longitude, latitude]),
        zoom: zoom
      })
    });
  }

  constructor() { }

  ngOnInit(): void {

    //this.afficherMap(this.zoom,this.longitude,this.latitude);
    //this.mettreMark(this.longitude, this.latitude);

    setTimeout(() => { this.geoFindMe(); }, 1000);
  }

}
