import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideosRecettesComponent } from './videos-recettes.component';

describe('VideosRecettesComponent', () => {
  let component: VideosRecettesComponent;
  let fixture: ComponentFixture<VideosRecettesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideosRecettesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideosRecettesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
