export interface Video {
    titre: string;
    lien: string;
    chaine: string;
    duree: string;
}