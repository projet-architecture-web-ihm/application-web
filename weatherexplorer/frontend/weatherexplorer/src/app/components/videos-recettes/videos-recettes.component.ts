import { Component, OnInit, Input } from '@angular/core';
import { Video } from './video';
import {TemperatureComponent} from '../temperature/temperature.component';

declare var gapi: any;


@Component({
  selector: 'app-videos-recettes',
  templateUrl: './videos-recettes.component.html',
  styleUrls: ['./videos-recettes.component.css']
})
export class VideosRecettesComponent implements OnInit {

  @Input() composantTemperature: TemperatureComponent;

  listeRecettes: Array<Video> = [];

  videoRecette: Video = {
    titre: "Recette inratable de crêpes - 750g",
    lien: "https://www.youtube.com/embed/_FJwht4OwGg",
    chaine: "750g",
    duree: "3:04"
  };

  creerRecettes = (listeRecettes: Array<Video>) => {
    let index = 0;
    listeRecettes.forEach(element => {
      this.creerUneSuggestion(element, index)
      index += 1;
    });
  }

  creerUneRecette = (recette: Video) => {

    let html = "<iframe src=" + recette.lien + " frameborder=\"0\" allowtransparency=\"true\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen style=\"height: 100%; width: 100%\"> </iframe>";

    let tbody = document.getElementsByClassName("chart-container-2")[0];

    tbody.appendChild(document.createElement("div"));

    tbody.children[0].innerHTML = html;

  }

  creerUneSuggestion = (recette: Video, index: number) => {
    let html = "<tr> <td> <i class=\"fa fa-circle text-white mr-2\"></i><a href=" + recette.lien + ">" + recette.titre + "</a><td>" + recette.chaine + "</td></tr>"//<td>" + recette.duree + "</td></tr>";

    let tbody = document.getElementById("videoRecette");

    tbody.appendChild(document.createElement("tr"));

    tbody.children[index].innerHTML = html;

  }

  constructor() { }

  traitement = (response: any) => {
    // Handle the results here (response.result has the parsed body).
    // console.log("Response", response.result);
    //Traitement de la réponse
    for (let i = 0; i < 5; i++) {
      // console.log(i + "VIDEO", response.result.items[i].id.videoId);
      //Autres suggestion
      if (i) {
        let video: Video = {
          titre: response.result.items[i].snippet.title,
          lien: "https://www.youtube.com/watch?v=" + response.result.items[i].id.videoId,
          chaine: response.result.items[i].snippet.channelTitle,
          duree: i + "VIDEO"
        }
        this.listeRecettes.push(video);
      }
      //Vidéo principal
      else {
        let video: Video = {
          titre: response.result.items[i].snippet.title,
          lien: "https://www.youtube.com/embed/" + response.result.items[i].id.videoId,
          chaine: response.result.items[i].snippet.channelTitle,
          duree: i + "VIDEO"
        }
        // console.log(video);
        this.creerUneRecette(video);
      }
    }
    this.creerRecettes(this.listeRecettes);
  }

  // BAPTISTE   : AIzaSyCl5BRNfqDi3ruSAinuUshoJF5MvRyRTWA
  // MARIE-NINA : AIzaSyAqvkzxtkwMC5lJ4MMg486qa5y_eLUksEk

  loadClient = () => {
    gapi.client.setApiKey("AIzaSyAqvkzxtkwMC5lJ4MMg486qa5y_eLUksEk");
    return gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
      .then(function () { console.log("GAPI client loaded for API"); },
        function (err) { console.error("Error loading GAPI client for API", err); });
  }
  // Make sure the client is loaded before calling this method.

  execute = (motClef) => {
    let fonction = this.traitement;
    return gapi.client.youtube.search.list({
      q: "recette " + motClef,
      part: 'id,snippet'
    })
      .then(function(response){fonction(response)}, function (err) { console.error("Execute error", err); });
  }

  ngOnInit(): void {
    gapi.load("client:auth2", function() {
      gapi.auth2.init({client_id: "455565391711-u8m74u1fkjgmlnhv8k6eks3mtj3he12d.apps.googleusercontent.com"});
    });

    setTimeout(() => {  this.loadClient(); }, 500);
    
    setTimeout(() => {  this.execute(this.composantTemperature.jsonData["city"]["name"]); }, 1500);
  }

}
